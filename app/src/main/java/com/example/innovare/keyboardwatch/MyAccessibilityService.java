package com.example.innovare.keyboardwatch;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.app.Service;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.KeyEvent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

public class MyAccessibilityService extends AccessibilityService {

    static final String TAG = MyAccessibilityService.class.getSimpleName();

    private void getEventType(AccessibilityEvent event) {
        switch (event.getEventType()) {
           /* case AccessibilityEvent.TYPE_NOTIFICATION_STATE_CHANGED:
                return "TYPE_NOTIFICATION_STATE_CHANGED";*/
           /* case AccessibilityEvent.TYPE_VIEW_CLICKED:
                return "TYPE_VIEW_CLICKED";*/
            /*case AccessibilityEvent.TYPE_VIEW_FOCUSED:
                return "TYPE_VIEW_FOCUSED";
            case AccessibilityEvent.TYPE_VIEW_LONG_CLICKED:
                return "TYPE_VIEW_LONG_CLICKED";*/
           /* case AccessibilityEvent.TYPE_VIEW_SELECTED:
                return "TYPE_VIEW_SELECTED";*/
           /* case AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED:
                return "TYPE_WINDOW_STATE_CHANGED";*/
            case AccessibilityEvent.TYPE_VIEW_TEXT_CHANGED:
               processData(event);
        }
     //   return "default";
    }

    private void processData(AccessibilityEvent event) {
        Log.d(TAG," Event is : "+event.getClassName());
        Log.d(TAG," Text changing is : "+getEventText(event));
        if (event.getClassName().equals("android.widget.EditText")){
            Log.d(TAG," You are changing an edittext ");
            AccessibilityNodeInfo source = event.getSource();
            /*if(source==null){
                return;
            }*/
            String beforeText = getEventText(event);
            String aftertext = beforeText;
            if (beforeText.contains("android")){
                Log.d(TAG," Came here to 1");
                aftertext = beforeText.replace("android","hacked");
            }
            Log.d(TAG," Came here to 2"+aftertext);
          //  ClipData clipData = ClipData.newPlainText("label",aftertext);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Bundle arguments = new Bundle();
                arguments.putCharSequence(AccessibilityNodeInfo
                        .ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE, aftertext);
                source.performAction(AccessibilityNodeInfo.ACTION_SET_TEXT, arguments);
            }else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("nfc_input", aftertext);
                clipboard.setPrimaryClip(clip);

                Log.d("SENDING DATA", Boolean.toString(source.refresh()));
                Log.d("SENDING DATA", Boolean.toString(source
                        .performAction(AccessibilityNodeInfo.ACTION_PASTE)));
                ClipData clip1 = ClipData.newPlainText("nfc_input", aftertext);
                clipboard.setPrimaryClip(clip1);
            }else {
                //For lower versions than API18
            }
        }
    }

    private String getEventText(AccessibilityEvent event) {
        StringBuilder sb = new StringBuilder();
        for (CharSequence s : event.getText()) {
            sb.append(s);
        }
        return sb.toString();
    }

    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
        /*Log.v(TAG, String.format(
                "onAccessibilityEvent: [type] %s [class] %s [package] %s [time] %s [text] %s",
                getEventType(event), event.getClassName(), event.getPackageName(),
                event.getEventTime(), getEventText(event)));*/
        getEventType(event);
    }

    @Override
    public void onInterrupt() {
        Log.v(TAG, "onInterrupt");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
       // return super.onStartCommand(intent, flags, startId);
        Log.d(TAG," On Start Command is called ");
        return START_STICKY;
    }

    @Override
    protected void onServiceConnected() {
        super.onServiceConnected();
        Log.v(TAG, "onServiceConnected");
        AccessibilityServiceInfo info = new AccessibilityServiceInfo();
        info.flags = AccessibilityServiceInfo.DEFAULT;
        info.eventTypes = AccessibilityEvent.TYPES_ALL_MASK;
        info.feedbackType = AccessibilityServiceInfo.FEEDBACK_GENERIC;
        setServiceInfo(info);
    }
}
